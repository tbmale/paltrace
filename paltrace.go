package paltrace

import (
	"image"
	"image/color"
	"image/color/palette"
	"os"

	"github.com/dennwc/gotrace"
	// "io"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

//GetChannels get all paths by Websafe pallete color in a list
func GetChannels(fpath string) map[color.Color][]gotrace.Path {
	imgs := make(map[color.Color][]gotrace.Path)
	r, err := os.Open(fpath)
	check(err)
	img, _, err := image.Decode(r)
	check(err)
	pal := make(map[int]*image.RGBA)
	gotrace.NewBitmapFromImage(img, func(x, y int, c color.Color) bool {
		//    r, g, b, _ := c.RGBA()
		i := color.Palette.Index(palette.WebSafe, c)
		_, err := pal[i]
		if !err {
			pal[i] = image.NewRGBA(img.Bounds())
		}
		pal[i].Set(x, y, palette.WebSafe[i])
		return false
	})
	for key, value := range pal {
		bm := gotrace.NewBitmapFromImage(value, nil)
		paths, _ := gotrace.Trace(bm, nil)
		imgs[palette.WebSafe[key]] = paths
	}
	return imgs
}
