package main

import (
	"fmt"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"os"

	"github.com/dennwc/gotrace"
	"gitlab.com/tbmale/paltrace"
	// "io"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	if len(os.Args) != 2 {
		return
	}
	fp, err := os.Open(os.Args[1])
	check(err)
	img, _, err := image.Decode(fp)
	check(err)
	bnd := img.Bounds()
	for key, value := range paltrace.GetChannels(os.Args[1]) {
		w, err := os.Create(fmt.Sprintf("%s-%d.svg", os.Args[1], key))
		check(err)
		r, g, b, _ := key.RGBA()
		gotrace.WriteSvg(w,
			bnd,
			value,
			fmt.Sprintf("#%x%x%x", uint8(r), uint8(g), uint8(b)))
		w.Close()
	}
	// for i,p := range value{

	// paths, _ := gotrace.Trace(bm, nil)
	// w,err := os.Create(os.Args[1]+".svg")
	// check(err)
	// gotrace.WriteSvg(w, img.Bounds(), paths, "")
	// }
}
